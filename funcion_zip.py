
nombres = ['Juliana', 'Pedro', 'Julian', 'Andrés', 'Sandy']
apellidos = ['Muñoz', 'Jimenez', 'López', 'Quintero']

nombres_apellidos = list(zip(nombres, apellidos))
print(nombres_apellidos)

id_cartas = ['A', '2', '3', '4']
corazones = list( map(lambda n: 'Corazones', id_cartas ) )
print(corazones)

cartas = list( zip(id_cartas, corazones) )
print(cartas)