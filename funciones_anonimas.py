
def func_sumar(n1,n2):
    return n1+n2

sumar = lambda n1,n2 : n1+n2
sumar(10,10)
#print( sumar(10,10) )

def fabricar_funcion(operador: str):
    funcion = lambda n1,n2: f'No existe operación para {n1} {operador} {n2}'
    if operador == '+':
        funcion = lambda n1,n2: n1+n2
    elif operador == '-':
        funcion = lambda n1,n2: n1-n2
    elif operador == '*':
        funcion = lambda n1,n2: n1*n2
    elif operador == '/':
        funcion = lambda n1,n2: n1/n2
    elif operador == '**':
        funcion = lambda n1,n2: n1**n2
    
    return funcion

respuesta = fabricar_funcion('%')
print( respuesta(10,2) )

""" operador = '+'
funcion = lambda n1,n2: f'No existe operación para {n1} {operador} {n2}'

print(funcion(10,10))
def miFuncion(n1,n2):
    return f'No existe operación para {n1} {operador} {n2}'

print(miFuncion(10,10)) """

""" sumar = lambda n1,n2 : n1+n2

func_superior = lambda funcion,n1,n2: funcion(n1,n2)

func_superior(sumar,10,20) """