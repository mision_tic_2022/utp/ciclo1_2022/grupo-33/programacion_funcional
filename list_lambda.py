
numeros = [10,20,30,40,50,60,70,80,90]

numeros_al_cuadrado = [n**2 for n in numeros ]
print(numeros_al_cuadrado)
print(numeros)


""" lista = []
for n in numeros:
    lista.append(n**2) """

'''
1) A partir de una lista de nombres obtener una lista con las iniciales 
['Andres', 'Juliana']
['A', 'J']
'''
lista = ["Luis", "Miguel"]
iniciales = [n[0] for n in lista]
print(iniciales)

obtener_iniciales = lambda lista: [ n[0] for n in lista ]

print( obtener_iniciales( lista ) )

