caracteres = ['á', 'í', 'ñ', 'ü']
#['a', 'i']

def eliminar_tildes(caracter: str):
    respuesta = caracter
    if caracter == 'á':
        respuesta = 'a'
    elif caracter == 'í':
        respuesta = 'i'
    return respuesta

caracteres_sin_tilde = list( map( eliminar_tildes, caracteres ) )
print(caracteres_sin_tilde)