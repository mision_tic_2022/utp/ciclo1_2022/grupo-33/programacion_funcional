from functools import reduce


lista_caracteres = ['H', 'O', 'L', 'A', ' ', 'M', 'U', 'N', 'D', 'O', ' ', 'M', 'I', 'S', 'I', 'O', 'N', ' ', 'T', 'I', 'C', ' ', '-', ' ', 'U', 'T', 'P']

'''
Concatenar todos los caracteres especiales de la lista.
'''
mensaje = reduce(lambda x,y :str(x)+str(y), lista_caracteres)
print(mensaje)


'''
A partir de la lista compuesta retorne la sumatoria de todos los elementos.
NOTA:
    Desarrollar la solución en 2 lineas de código
'''
lista_compuesta = [ [100,200,300], [400,500,600,700], [800,900,1000], [150,260,850] ]

#lista_sumatoria = list(map( lambda numeros: reduce(lambda ac,e: ac+e, numeros) , lista_compuesta))
#print(reduce( lambda ac,e: ac+e, lista_sumatoria ))

resp = reduce(lambda ac,e: ac+e, list(map( lambda numeros: reduce(lambda ac,e: ac+e, numeros) , lista_compuesta)))
print(resp)