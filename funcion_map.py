
numeros = [10,20,30,40,50]

elevar = lambda n: n**2

numeros_al_cuadrado = list(map(elevar, numeros))
print(numeros_al_cuadrado)


def simulacion_map(funcion, lista):
    respuesta = []
    for n in lista:
        num = funcion(n)
        respuesta.append(num)
    return respuesta

print( simulacion_map(elevar, numeros) )

print('------------------')

lista_mensajes = ['Hola', 'mundo', 'utp', 'mision', 'tic']

inicialies = list(map( lambda m : m[0], lista_mensajes ))
print(inicialies)

mayuscula = tuple( map( lambda mensaje: mensaje.upper(), lista_mensajes) )
print(mayuscula)

print('--------')

def obtener_caracteres(mensaje: str):
    primer_caracter = mensaje[0]
    ultimo_caracter = mensaje[ len(mensaje)-1 ]
    return f'{primer_caracter} - {ultimo_caracter}'

respuesta = list( map( obtener_caracteres, lista_mensajes ) )
print(respuesta)