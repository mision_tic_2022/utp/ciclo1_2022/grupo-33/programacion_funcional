
def fabricar_funcion(operador: str):
    def default(n1, n2):
        return f"La operación no existe para {n1} {operador} {n2}"
    funcion = default
    if operador == '+':
        def sumar(n1,n2):
            return n1+n2
        funcion = sumar
    elif operador == '-':
        def restar(n1,n2):
            return n1-n2
        funcion = restar
    elif operador == '*':
        def multiplicar(n1,n2):
            return n1*n2
        funcion = multiplicar
    elif operador == '/':
        def dividir(n1,n2):
            return n1/n2
        funcion = dividir
    
    return funcion


def ejecutar_operaciones(funcion_1, funcion_2):
    n1 = int( input('Ingrese n1: ') )
    n2 = int( input('Ingrese n2: ') )
    resultado_1 = funcion_1(n1, n2) 
    resultado_2 = funcion_2(n1, n2)
    print( f'Resultado funcion_1: {resultado_1}' )
    print(f'Resultado funcion_2: {resultado_2}')

def solicitar_operacion():
    operador_1 = input('Ingrese la operación a crear: ')
    operador_2 = input('Ingrese la segunda operación a crear: ')
    funcion_1 = fabricar_funcion(operador_1)
    funcion_2 = fabricar_funcion(operador_2)
    ejecutar_operaciones(funcion_1, funcion_2)

solicitar_operacion()

