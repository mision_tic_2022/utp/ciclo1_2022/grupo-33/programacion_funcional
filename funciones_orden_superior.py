
def sumar(n1,n2):
    return n1+n2

def funcion_orden_superior(funcion):
    respuesta = funcion(20,20)
    print(respuesta)

def concatenar(n1,n2):
    return f'{n1}+{n2}'

funcion_orden_superior(concatenar)

