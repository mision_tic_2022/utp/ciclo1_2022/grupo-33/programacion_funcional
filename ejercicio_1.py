'''
1) Desarrolle en una linea de código (utilizando funciones anónimas) 
    una función para calcular el promedio de una lista de números 
    (Puede utilizar sum())
2) Desarrolle una función que reciba como parámetro un String y retorne 
    el primer caracter
3) Desarrolle una función que reciba como parámetro un String y retorne 
    el último caracter
4) Desarrolle una función que reciba como parámetro una lista de tamaño 10,
    retorne una lista con los elementos de la posición 5 en adelante
NOTA:
    Para todos los puntos desarrollar las soluciones en una sola línea de código
'''

#Punto #1
promedio = lambda lista_numeros: sum(lista_numeros) / len(lista_numeros)
numeros = [4.5,4.8,3.2,4.1]
print( promedio(numeros) )

#Punto #2
funcion2= lambda palabra: palabra[0]
print('Punto #2:\n', funcion2('Hola mundo'))

#Punto #3
devolver_palabra = lambda ultimaLetra: ultimaLetra[-1]
print('Punto #3:\n', devolver_palabra('Hola mundo'))

#Punto #4
lista = [0,1,2,3,4,5,6,7,8,9]
rebanado = lambda lista : lista[5:]
print('Punto#5:\n', rebanado(lista))

'''
Configurar vsCode para que no convierta las funciones lambda a def:
Oprimir ctrl + -> autopep8 ->  agregar elemento -> escribir: --ignore=E731
O simplemente encerrar lambda en parentesis y tambien funciona.
'''