
nombres = ['Milfer','Royer', 'Juliana', 'María', 'Jorge', 'Juan']

nombres_x_j = [n for n in nombres if n[0].upper() == 'J' ]
print(nombres_x_j)

numeros = [10,15,20,25,30,35,40,45,50,55]
numeros_pares = [n for n in numeros if n%2==0]
print(numeros_pares)

print('------if else-------')
nombres_x_j_mayuscula = [n.upper() if n[0].lower()=='j' else n.lower() for n in nombres]
print(nombres_x_j_mayuscula)

print('------------------------------------------')
lista = []
for n in nombres:
    if n[0].lower() == 'j':
        lista.append( n.upper() )
    else:
        lista.append( n.lower() )

print(lista)

mensaje = ''.join(lista)
print(mensaje)

