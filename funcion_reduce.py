
from functools import reduce


numeros = list( range(20) )
print(numeros)

suma = 0
for n in numeros:
    suma += n

print(suma)

funcion_sumar = lambda suma, elemento: suma+elemento

print(reduce(lambda suma, n: suma+n, numeros))

nombres = ['Juan', 'Milfer', 'Juliana', 'Sandy']

respuesta = reduce( lambda ac,e: ac+'-'+e, nombres  )
print(respuesta)


numeros = [1,2,3,4,5]
#1,2,3,4,5

respuesta = reduce( lambda ac,e: str(ac)+','+str(e), numeros )
print(respuesta)

